(function ($, window) {
    function ajustarVisibilidadMenu(){		
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + ($(window).height());
        var elemTop = $("#top_menu").offset().top;
        var elemBottom = elemTop + $("#top_menu").height();
        
        if (elemTop <= docViewTop && docViewTop > 0){                        
            $("#top_menu").addClass("menu-fijo");
        }
        else{
            $("#top_menu").removeClass("menu-fijo");        
        }
    }
    
    $(document).ready(function () {
        if ($("#banner_home").size() > 0){
            $('#banner_home').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                slideMargin: 20,
                pager: true,
                nextText: '',
                prevText: '',
                moveSlides: 1,
                infiniteLoop: myslider_loop,
                autoHover: true,
                auto: myslider_loop,
                speed: parseInt(myslider_speed),
                pause: myslider_pause,
                controls: true
            });
        }
        
        if ($(".carrusel-productos ul").size() > 0){
            $('.carrusel-productos ul').bxSlider({
                minSlides: 1,
                maxSlides: 2,
                slideMargin: 5,
                pager: true,
                nextText: '',
                prevText: '',
                moveSlides: 1,
                controls: true,
                adaptiveHeight: true,
                slideWidth: 270,
                infiniteLoop: true
            });
        }
        
        /*$('.matching ul.product_list').bxSlider({
            minSlides: 1,
            maxSlides: 5,
            slideMargin: 20,
            pager: false,
            nextText: '',
            prevText: '',
            moveSlides: 1,
            autoHover: true,
            controls: true
        });*/
        
        $("a[rel=ajax_prod]").click(function (e) {
            e.preventDefault();
            $.fancybox({href: $(this).attr("data-url"), type: 'ajax', autoSize: false, maxWidth: '500px', scrolling: 'no', arrows: false, wrapCSS: 'my-fancy', title: null});
        });
        
        $(".despl-resp a").click(function(e) {
            e.preventDefault();
            var $target = $(this).attr("href");
            $($target).toggle();
        });
        
        $(window).scroll(function () {
            ajustarVisibilidadMenu();
        });
        
        ajustarVisibilidadMenu();
    });
})(jQuery, window);
