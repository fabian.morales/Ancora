<div class="container">
    <div class="row">
        <div class="col-md-12">
            <img src="{$link->getCatImageLink($catMatching->link_rewrite, $catMatching->id_image, 'ancat_big')|escape:'html':'UTF-8'}" class="img-responsive" />
            <br />
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            {hook h='displayInnerLeftColumn'}
        </div>
        <div class="col-md-9 texto_mat">
            <h1 class="titulo_mat">{$catMatching->name}</h1>
            {foreach from=$catsMatching item=cat}
                <div class="row separador-bottom doble">
                    <div class="col-md-7">
                        <a href="{$base_dir}index.php?controller=custom&tarea=mostrar_matching_det&id={$cat->id}">
                            <img src="{$link->getCatImageLink($cat->link_rewrite, $cat->id_image, 'an_cat_std')|escape:'html':'UTF-8'}" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-md-5">
                        <h2 class="titulo_mat"><a href="{$base_dir}index.php?controller=custom&tarea=mostrar_matching_det&id={$cat->id}">{$cat->name}</a></h2>
                        <p class="texto_mat">{$cat->description}</p>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</div>