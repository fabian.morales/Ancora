<div class="container">
    <div class="row">
        <div class="col-md-12 historia">
            <div class="row">
                <div class="col-md-6">
                    <h3>Nuestra historia</h3>
                    <p>De la unión de una mente creativa y una con ideas inexploradas, nace en Colombia Ancora SM. 
                        Desde el inicio, Ancora SM se ha caracterizado por ser una marca conceptual, pionera y 
                        totalmente enfocada en conceptualizar un mismo estilo de vida elegante e innovador para 
                        hombres y mujeres. El slogan "Dressed to Swim" o “Vestido para nadar” hace un llamado a el 
                        uso de las tendencias urbanas en los vestidos de baño tanto para hombre como para mujer</p>
                </div>
                <div class="col-md-6">
                    <h3>Nuestro producto</h3>
                    <p>Los productos Ancora se diferencian por la exclusividad de sus estampados, y 
                        por la introducción del concepto “Matching Pair” en Colombia, donde los 
                        vestidos de baño de mujer se complementan con las pantalonetas de baño 
                        para hombres. Cada producto es 100% colombiano elaborado por madres cabezas 
                        de familia. Ancora se mantiene a la vanguardia de las tendencias urbanas y 
                        de la moda aplicando en cada una de sus prendas calidad, diseño y exclusividad.</p>
                </div>
            </div>
            <br />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="titulo_cat historia">{$catMatching->name}</h1>
            {foreach from=$catsMatching item=cat}
                <div class="row separador-bottom doble">
                    <div class="col-md-7">
                        <img src="{$link->getCatImageLink($cat->link_rewrite, $cat->id_image, 'an_cat_std')|escape:'html':'UTF-8'}" class="img-responsive" />
                    </div>
                    <div class="col-md-5">
                        <h2 class="titulo_cat historia">{$cat->name}</h2>
                        <p>{$cat->description}</p>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</div>