<div class="container">
    <div class="row">
        <div class="col-md-12">
            <img src="{$link->getCatImageLink($catMatching->link_rewrite, $catMatching->id_image, 'ancat_big')|escape:'html':'UTF-8'}" class="img-responsive" />
            <br />
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            {hook h='displayInnerLeftColumn'}
        </div>
        <div class="col-md-9 matching">
            {foreach from=$productos item=coleccion}
                {include file="../product-list.tpl" products=$coleccion}
            {/foreach}
        </div>
    </div>
</div>