{if $cats|@count > 0}
<ul class="lista-cat">
    {if $primerNivel == 1}
        <li><a href="{$base_dir}index.php">Inicio</a></li>
    {/if}
    {foreach from=$cats item=cat}
    {if $cat.hijas|@count > 0}
    <li class="padre">
    {else}
    <li>
    {/if}
        <a href="{$link->getCategoryLink($cat.id_category, $cat.link_rewrite)|escape:'html':'UTF-8'}">
            {$cat.name|escape:'html':'UTF-8'}
        </a>
        {include file="./categorias-arbol.tpl" cats=$cat.hijas primerNivel=0}
    </li>
    {/foreach}
    {if $primerNivel == 1}
        <li><a href="{$base_dir}index.php">Encuentra un Ancora</a></li>
        <li><a href="{$base_dir}index.php">Nosotros</a></li>
        <li><a href="{$base_dir}index.php">Contacto</a></li>
    {/if}
</ul>
{/if}