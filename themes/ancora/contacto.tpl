<div class="row">
    <div class="col-md-12">
        <img class="img-responsive img-fullw" src="{$base_dir}themes/ancora/imagenes/contacto.jpg" />
    </div>
</div>
<br />
<div class="formulario_contacto_full">
    <form method="post" action="{$base_dir}index.php?controller=custom&tarea=enviar_contacto">
        <div class="row">
            <div class="col-md-6 text-center" style="margin-top: 70px;">
                <div><img src="{$base_dir}themes/ancora/imagenes/celular.png"/>&nbsp; <span class="info-contacto"><strong>(+57) 3157264902</strong></span></div>
                <div><img src="{$base_dir}themes/ancora/imagenes/telefono.png"/>&nbsp; <span class="info-contacto"><strong>(+57) 3202383798</strong></span></div>
                <div><img src="{$base_dir}themes/ancora/imagenes/mail.png"/>&nbsp; <span class="info-contacto">info@ancoraswimwear.com </span></div>
                <div><img src="{$base_dir}themes/ancora/imagenes/pin.png"/>&nbsp;<span class="info-contacto">Bogot&aacute;, Colombia</span></div>
            </div>
            <div class="col-md-6">
                <div class="page-subheading">Contáctanos</div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" placeholder="Nombre" />
                    </div>
                    <div class="col-md-12">
                        <input type="text" placeholder="Correo eletrónico" />
                    </div>
                    <div class="col-md-12">
                        <input type="text" placeholder="Teléfono de contacto" />
                    </div>
                    <div class="col-md-12">
                        <textarea rows="5" placeholder="¿En qué podemos ayudarte?"></textarea>
                    </div>
                    <div class="col-md-12 text-center">
                        <button class="btn btn-default button button-medium">Enviar</button>
                        {*<span class="right">* Campos requeridos</span>*}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<br />