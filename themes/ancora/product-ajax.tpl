<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id="image-block" class="clearfix">
                {if isset($imagenes) && count($imagenes) > 0 }
                    <div class="row">
                        {foreach from=$imagenes item=image name=thumbnails key=k}
                            {assign var=imageIds value="`$producto->id`-`$image.id_image`"}
                            {if !empty($image.legend)}
                                {assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
                            {else}
                                {assign var=imageTitle value=$producto->name|escape:'html':'UTF-8'}
                            {/if}

                            {if count($imagenes) == 2}
                                {if $smarty.foreach.thumbnails.index == 0}
                                    <div class="col-xs-12 col-md-6">
                                        <a href="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_big')|escape:'html':'UTF-8'}">
                                            <img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_big')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" itemprop="image" />
                                        </a>
                                    </div>
                                {/if}
                            
                                {if $smarty.foreach.thumbnails.index == 1}
                                    <div class="col-xs-12 col-md-6">
                                        <a href="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_big')|escape:'html':'UTF-8'}">
                                            <img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_big')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" itemprop="image" />
                                        </a>
                                    </div>
                                {/if}
                            {else}
                                {if $smarty.foreach.thumbnails.index == 0}
                                    <div class="col-xs-12 col-md-8">
                                        <a href="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_big')|escape:'html':'UTF-8'}">
                                            <img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_big')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" itemprop="image" />
                                        </a>
                                    </div>
                                {/if}
                            
                                {if $smarty.foreach.thumbnails.index == 1}
                                    <div class="col-xs-12 col-md-4">
                                        <div class="row">
                                            <div class="col-md-12 primer item gal">
                                                <a href="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_big')|escape:'html':'UTF-8'}">
                                                    <img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_small')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" itemprop="image" />
                                                </a>
                                            </div>
                                {/if}

                                {if $smarty.foreach.thumbnails.index == 2}
                                            <div class="col-md-12">
                                                <a href="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_big')|escape:'html':'UTF-8'}">
                                                    <img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($producto->link_rewrite, $imageIds, 'an_prod_small')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" itemprop="image" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                            {/if}

                        {/foreach}
                    </div>
                {/if}
            </div>
        </div>
    </div>
    <div class="row relative">
        <div class="col-md-8">
            <div class="nombre detalle_producto">{$producto->name|escape:'html':'UTF-8'}</div>
            {if isset($producto->id_category_default)}
                {assign var='catname' value=Category::getCategoryInformations(array($producto->id_category_default))}
                <div class="descripcion_categoria">{$catname[$producto->id_category_default].name}</div>
            {/if}
            
            <div class="precio_ajax">${$producto->getPrice(true, $smarty.const.NULL, 6)|round:2} COP</div>
        </div>
        <div class="col-md-4">
            <div class="bloque-ver-mas">
                <a class="ver-mas" href="{$link->getProductLink($producto)|escape:'html':'UTF-8'}">Ver m&aacute;s</a>
            </div>
        </div>
    </div>
</div>
