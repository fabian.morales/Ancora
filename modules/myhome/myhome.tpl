{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo home -->
<div class="container separador-top">
    <div class="row">
        {foreach from=$categorias item=categoria}
            <div class="col-md-4">
                <a href="{$link->getCategoryLink($categoria->id_category, $categoria->link_rewrite)|escape:'html':'UTF-8'}" class="cat_home">
                    <img class="img-responsive" src="{$link->getCatImageLink($categoria->link_rewrite, $categoria->id_image, 'an_cat_home')|escape:'html':'UTF-8'}" />
                    <div class="descripcion-home-cat">{$categoria->name}</div>
                </a>
            </div>
        {/foreach}
    </div>
</div>
<div class="container separador-top">
    <div class="row">
        <div class="col-md-6"><img class="img-responsive" src="{$base_dir}themes/ancora/imagenes/promo_01.jpg" /></div>
        <div class="col-md-6"><img class="img-responsive" src="{$base_dir}themes/ancora/imagenes/promo_02.jpg" /></div>
    </div>
</div>
