<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="menu_hrz clearfix">
                <div class="despl-resp hidden-lg hidden-md">
                    <a href="#menu_horz">Menu</a>
                </div>  

                <ul id="menu_horz" class="clearfix menu-resp">
                    <li class="fixed-logo"><img src="{$base_dir}themes/ancora/imagenes/fixed-logo.png" /></li>
                    <li><a href="{$base_dir}index.php" title="Inicio">Inicio</a></li>
                    <li>
                        <a href="{$base_dir}12-Mujer">Mujer</a>
                        <div class="submenu">
                            <div class="row">
                                <div class="col-sm-3">
                                    <ul class="nav">
                                    {foreach from=$mujer item=categoria}
                                        <li>
                                            <a href="{$link->getCategoryLink($categoria->id_category, $categoria->link_rewrite)|escape:'html':'UTF-8'}">
                                                {$categoria->name}
                                            </a>
                                        </li>
                                    {/foreach}
                                    </ul>
                                </div>
                                <div class="col-sm-9 hidden-xs">
                                    <ul class="row">
                                    {foreach from=$mujer item=categoria}
                                        <li class="col-md-12">
                                            <a href="{$link->getCategoryLink($categoria->id_category, $categoria->link_rewrite)|escape:'html':'UTF-8'}">
                                                <img class="img-responsive" src="{$link->getCatImageLink($categoria->link_rewrite, $categoria->id_image, 'an_cat_std')|escape:'html':'UTF-8'}" />
                                            </a>
                                        </li>
                                        {break}
                                    {/foreach}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="{$base_dir}35-Accesorios">Accesorios</a></li>
                    <li><a href="{$base_dir}13-Hombre">Hombre</a></li>
                    <li><a href="{$base_dir}32-Ninos">Niños</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=mostrar_matching_home">Matching</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=mostrar_mapa">Encuentra un Ancora</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=mostrar_historia">Ancora en la historia</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=mostrar_contacto">Contacto</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

