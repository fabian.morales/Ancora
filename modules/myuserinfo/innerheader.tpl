<div class="row">
    <div class="col-sm-12">
        <img src="{$base_dir}/themes/pandora/imagenes/banner_inner.jpg" class="img-responsive img-fullw" />
    </div>
</div>
<div class="row userinfo">
    <div class="col-md-4">Shop</div>
    <div class="col-md-5">
        <img src="{$base_dir}/themes/pandora/imagenes/usuario.png" class="icono_cuenta" />
        {if $logged}
            <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="Ver mi cuenta" class="userinfo micuenta" rel="nofollow"><span>{$cookie->customer_firstname}</span></a>
        {else}
            <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="Iniciar sesi&oacute;n" class="userinfo login" rel="nofollow">Login</a>
        {/if}
    </div>
    <div class="col-md-1">
        <a href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|addslashes}" title="Dreambox" rel="nofollow">
            <img src="{$base_dir}/themes/pandora/imagenes/dreambox.png" />
        </a>
    </div>
    <div class="col-md-2">
        <div class="userinfo menu">Menu</div>
    </div>
</div>