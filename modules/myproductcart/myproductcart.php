<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_'))
	exit;

class MyProductCart extends Module
{
    public function __construct()
    {
        $this->name = 'myproductcart';
        if (version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
            $this->tab = 'front_office_features';
        else
            $this->tab = 'Blocks';
        $this->version = '1.0.0';
        $this->author = 'Fabian Morales';

        $this->bootstrap = true;
        parent::__construct();	

        $this->displayName = 'My Product Cart';
        $this->description = 'Funcionalidades adicionales del carrito';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install() &&			
            $this->registerHook('displayProductExtraRight') &&
            $this->registerHook('header');
    }


    public function uninstall()
    {
        // Delete configuration
        parent::uninstall();
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS(($this->_path).'mycontacto.css', 'all');
    }

    public function hookDisplayProductExtraRight($params)
    {
        if (!empty($params["product"])){
            $producto = $params["product"];

            $accesorios = $producto->getAccessories((int)($this->context->language->id));
            
            $tops = array();
            $bottoms = array();
            $link = new Link();
            
            foreach ($accesorios as $a){
                $categorias = Product::getProductCategories($a["id_product"]);
                $p = new Product($a["id_product"], false, (int)($this->context->language->id));
                $image = Image::getCover($p->id);
                $imagePath = $link->getImageLink($p->link_rewrite, $image['id_image'], 'an_prod_std');
                
                if (in_array(39, $categorias)){
                    $tops[] = array("producto"=>$p, "imagen"=>$imagePath, "attr" => $p->getAttributesGroups((int)($this->context->language->id)));
                }
                
                if (in_array(40, $categorias)){
                    $bottoms[] = array("producto"=>$p, "imagen"=>$imagePath, "attr" => $p->getAttributesGroups((int)($this->context->language->id)));
                }
            }
            
            $this->smarty->assign('tops', $tops);
            $this->smarty->assign('bottoms', $bottoms);
            
            return $this->display(__FILE__, 'myproductcart.tpl');
        }
        //$this->context->controller->addCSS($this->_path.'style.css', 'all');
        //return $this->display(__FILE__, 'mycontacto.tpl');
    }
}
