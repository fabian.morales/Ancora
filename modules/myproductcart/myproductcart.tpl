{if $tops|@count > 0 && $bottoms|@count > 0}
<div class="row partes_prod">
    <div class="col-sm-12">
        <div class="titulo_parte">Escoge la pieza que más te guste y combínala</div>
    </div>
    <div class="col-sm-12">
        <div class="subtitulo_parte">Top</div>
    </div>
    {foreach from=$tops item=top}
        <div class="col-md-6 relative">
            <select id="id_attribute_prod_{$top.producto->id}">
            {foreach from=$top.attr item=attr}
                <option value="{$attr.id_product_attribute}">{$attr.attribute_name}</option>
            {/foreach}
            </select>
            <img class="img-responsive" src="http://{$top.imagen|escape:'html':'UTF-8'}"  />
            <input class="product_cart" type="radio" id="sel_{$attr.id_product_attribute}" name="sel[{$attr.id_product_attribute}]" />
        </div>
    {/foreach}
</div>

<div class="row partes_prod">
    <div class="col-sm-12">
        <div class="subtitulo_parte">Bottom</div>
    </div>
    {foreach from=$bottoms item=bottom}
        <div class="col-md-6 relative">
            <select id="id_attribute_prod_{$bottom.producto->id}">
            {foreach from=$bottom.attr item=attr}
                <option value="{$attr.id_product_attribute}">{$attr.attribute_name}</option>
            {/foreach}
            </select>
            <img class="img-responsive" src="http://{$bottom.imagen|escape:'html':'UTF-8'}"  />
            <input class="product_cart" type="radio" id="sel_{$attr.id_product_attribute}" name="sel[bottom]" value="{$attr.id_product_attribute}" />
        </div>
    {/foreach}
</div>
{/if}