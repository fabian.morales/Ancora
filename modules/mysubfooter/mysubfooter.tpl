{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo sub-footer -->
{if $page_name == 'index'}
    {*<footer id="footer">
        <div class="container">
            <div class="row">
                
            </div>
        </div>
    </footer>*}
{else}
    
{/if}
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center lateral">
                <div class="titulo">Síguenos</div>
                <ul class="redes">
                    <li><a href="#"><img src="{$base_dir}themes/ancora/imagenes/facebook.png" /></a></li>
                    <li><a href="#"><img src="{$base_dir}themes/ancora/imagenes/twitter.png" /></a></li>
                    <li><a href="#"><img src="{$base_dir}themes/ancora/imagenes/instagram.png" /></a></li>
                </ul>
                <div class="clearfix"></div>
                <div>
                    <span class="titulo_newsletter">Newsletter</span>
                    <input id="txtNewsletter" type="text" placeholder="SUSCRÍBETE PARA RECIBIR LO MEJOR DE ANCORA" />    
                </div>
                
            </div>
            <div class="col-md-3 text-center lateral">
                <div class="titulo">Servicio al cliente</div>
                <nav>
                    <ul>
                        <li><a href="#">Condiciones de envío</a></li>
                        <li><a href="#">Reembolso</a></li>
                        <li><a href="#">Pagos</a></li>
                        <li><a href="#">Tabla de medidas</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3 text-center lateral">
                <div class="titulo">Garantías</div>
                <nav>
                    <ul>
                        <li><a href="#">Pago seguro</a></li>
                        <li><a href="#">Políticas de privacidad</a></li>
                        <li><a href="#">Términos y condiciones</a></li>
                        <li><a href="#">Créditos</a></li>
                        <li><a href="#">Cookies</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3 text-center">
                <img src="{$base_dir}themes/ancora/imagenes/logo.png" class="img-responsive logo-footer" />
            </div>
        </div>
    </div>
    <div class="copy">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center" style="font-size: 1.1rem">
                    2016 &copy; Ancora SM, Todos los derechos reservados
                </div>
            </div>
        </div>
    </div>
</footer>